import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  public cartItemList : any =[];
  public key: any;
  constructor() { }
 addCart(item: any){
  this.cartItemList.push(item);
 }

 getCart() {
   return this.cartItemList;
   console.log(this.cartItemList);
 }
 
 setKey(key:any) {
   this.key = key;
 }

 getKey() {
   return this.key;
 }
}
