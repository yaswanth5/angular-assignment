import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
 products: any = [];
 grandTotal = [];
 item = [];

  constructor(private service: ServiceService) { }

  ngOnInit(): void {
    this.products =  this.service.getCart(); 
    console.log(this.products);
  }

  emptycart() {
    this.products =[];
  }
  removeItem(index: any) {
    this.products.splice(index,1);
  }
}
