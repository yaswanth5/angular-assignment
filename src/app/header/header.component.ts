import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  key: any;
  constructor( private service: ServiceService) { }

  ngOnInit(): void {
  }

  search(event:any)
  {
    this.key = (event.target as HTMLInputElement).value;
    console.log(this.key);
  }

  setKey() {
    this.service.setKey(this.key);
  }
}
