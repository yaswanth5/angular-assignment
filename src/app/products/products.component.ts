import { Component, OnInit } from '@angular/core';
import Data from '../data.json';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  items: any = [] = Data;
  key : any = this.service.getKey();
  constructor(private service: ServiceService) { }

  ngOnInit(): void {
    this.key = this.service.getKey();
    console.log(this.key);
  }

  add(item:any) {
    this.service.addCart(item);
  }
 
 
}
